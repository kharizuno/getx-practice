import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getx/LifeCycle/controllers/countController.dart';
import 'package:getx/LifeCycle/controllers/textController.dart';

class LifeCycle extends StatelessWidget {
  final countC = Get.put(CountController());

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text('Home Page'),
          actions: [
            // Navigator Native Flutter
            // IconButton(
            //   onPressed: () =>
            //       Navigator.of(context).push(MaterialPageRoute(
            //     builder: (context) => OtherPage(),
            //   )),
            //   icon: Icon(Icons.refresh),
            // )

            // Navigator Getx
            IconButton(
              onPressed: () => Get.to(() => OtherPage()),
              icon: Icon(Icons.refresh),
            )
          ],
        ),
        body: Center(
          child: CountWidget(),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            countC.increment();
          },
        ),
      ),
    );
  }
}

class CountWidget extends StatelessWidget {
  // final countC = Get.find<CountController>();

  @override
  Widget build(BuildContext context) {
    return GetBuilder<CountController>(
      init: CountController(),
      initState: (_) => print('initState'),
      didChangeDependencies: (state) => print('didChangeDependencies'),
      didUpdateWidget: (oldWidget, state) => print('didUpdateWidget'),
      dispose: (state) => print('dispose'),
      builder: (_) {
        return Text('Angka ${_.count.age.value}');
      },
    );
  }
}

class OtherPage extends StatelessWidget {
  final textC = Get.put(TextController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Other Page'),
        ),
        body: TextField(
          controller: textC.myC,
        ));
  }
}
