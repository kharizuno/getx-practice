import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getx/LifeCycle/models/countModel.dart';

class CountController extends GetxController {
  var count = CountModel();

  void increment() {
    count.age.value++;
    update();
  }
}