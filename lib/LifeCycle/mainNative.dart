import 'package:flutter/material.dart';

class LifeCycle extends StatefulWidget {
  @override
  State<LifeCycle> createState() => _LifeCycleState();
}

class _LifeCycleState extends State<LifeCycle> {
  var count = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
            onPressed: () =>
                Navigator.of(context).pushReplacement(MaterialPageRoute(
              builder: (context) => OtherPage(),
            )),
            icon: Icon(Icons.refresh),
          )
        ],
      ),
      body: Center(
        child: CountWidget(count: count),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            count++;
          });
        },
      ),
    );
  }
}

class CountWidget extends StatefulWidget {
  int count;

  CountWidget({
    Key? key,
    required this.count,
  }) : super(key: key);

  @override
  _CountWidgetState createState() => _CountWidgetState();
}

class _CountWidgetState extends State<CountWidget> {
  @override
  void initState() {
    print('initState');
    super.initState();
  }

  // componentDidMount
  @override
  void didChangeDependencies() {
    print('didChangeDependencies');
    super.didChangeDependencies();
  }

  // componentDidUpdate
  @override
  void didUpdateWidget(covariant CountWidget oldWidget) {
    print('Old Widget : $oldWidget == didUpdateWidget');
    super.didUpdateWidget(oldWidget);
  }

  // componentWillUnmount
  @override
  void dispose() {
    print('dispose');
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Text('Angka ${widget.count}');
  }
}

class OtherPage extends StatelessWidget {
  const OtherPage({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
    );
  }
}