import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PageOne extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Page One'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton.icon(
              onPressed: () {
                // Get.to(PageTwo());
                Get.toNamed('/page-2');
              },
              icon: Icon(Icons.navigate_next_outlined),
              label: Text('Next Page'),
            ),
          ],
        ),
      ),
    );
  }
}
