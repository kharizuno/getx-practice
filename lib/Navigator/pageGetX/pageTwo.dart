import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PageTwo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Page Two'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton.icon(
              onPressed: () {
                Get.back();
              },
              icon: Icon(Icons.navigate_before_outlined),
              label: Text('Previous Page'),
            ),
            ElevatedButton.icon(
              onPressed: () {
                // Get.to(PageThree());
                Get.toNamed('/page-3');
              },
              icon: Icon(Icons.navigate_next_outlined),
              label: Text('Next Page'),
            ),
          ],
        ),
      ),
    );
  }
}
