import 'package:flutter/material.dart';
import 'package:getx/Navigator/pageNative/pageTwo.dart';

class PageOne extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Page One'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => PageTwo(),
                  ),
                );
              },
              style: ElevatedButton.styleFrom(
                fixedSize: Size(150, 50)
              ),
              child: Text('Next Page'),
            ),
          ],
        ),
      ),
    );
  }
}
