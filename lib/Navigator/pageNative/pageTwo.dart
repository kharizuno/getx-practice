import 'package:flutter/material.dart';
import 'package:getx/Navigator/pageNative/pageThree.dart';

class PageTwo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Page Two'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              style: ElevatedButton.styleFrom(
                fixedSize: Size(150, 50)
              ),
              child: Text('Previous Page'),
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => PageThree(),
                  ),
                );
              },
              style: ElevatedButton.styleFrom(
                fixedSize: Size(150, 50)
              ),
              child: Text('Next Page'),
            ),
          ],
        ),
      ),
    );
  }
}
