import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:getx/routes/routeName.dart';

class Product extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Product Page'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              onPressed: () {
                Get.toNamed(RouteName.product + '/1/apple?type=fruit');
              },
              child: Text('Product 1'),
            ),
            ElevatedButton(
              onPressed: () {
                Get.toNamed(RouteName.product + '/2/car?type=otomotive');
              },
              child: Text('Product 2'),
            ),
            ElevatedButton(
              onPressed: () {
                Get.toNamed(RouteName.product + '/3/shopee?type=online-shop');
              },
              child: Text('Product 3'),
            ),
          ],
        ),
      ),
    );
  }
}