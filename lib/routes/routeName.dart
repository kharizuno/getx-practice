abstract class RouteName {
  static const home = "/";

  // Folder DynamicURL
  static const product = '/product';

  // Folder GetConnect
  static const add = "/add";
  static const profile = "/profile";
}