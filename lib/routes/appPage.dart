import 'package:get/get.dart';
import 'package:getx/routes/routeName.dart';

import 'package:getx/Navigator/pageGetX/pageOne.dart';
import 'package:getx/Navigator/pageGetX/pageTwo.dart';
import 'package:getx/Navigator/pageGetX/pageThree.dart';

import 'package:getx/DynamicUrl/pages/product.dart';
import 'package:getx/DynamicUrl/pages/detail.dart';

import 'package:getx/GetConnect/bindings/addBinding.dart';
import 'package:getx/GetConnect/bindings/profileBinding.dart';
import 'package:getx/GetConnect/views/addPage.dart';
import 'package:getx/GetConnect/views/homePage.dart';
import 'package:getx/GetConnect/views/profilePage.dart';

class AppPage {
  static final pages = [
    GetPage(name: RouteName.home, page: () => HomePage()),

    // Folder Navigator
    GetPage(name: '/page-1', page: () => PageOne()),
    GetPage(name: '/page-2', page: () => PageTwo()),
    GetPage(name: '/page-3', page: () => PageThree()),

    // Folder DynamicUrl
    GetPage(name: RouteName.product, page: () => Product()),
    GetPage(name: RouteName.product + '/:id/:name?', page: () => ProductDetail()),

    // Folder GetConnect
    GetPage(
      name: RouteName.profile,
      page: () => ProfilePage(),
      binding: ProfileBinding(),
    ),
    GetPage(
      name: RouteName.add,
      page: () => AddPage(),
      binding: AddUserBinding(),
    ),
  ];
}