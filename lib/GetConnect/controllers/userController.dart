import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getx/GetConnect/apis/userApi.dart';
import 'package:getx/GetConnect/models/userModel.dart';

class UserController extends GetxController {
  var user = User(id: '', name: '', email: '', phone: '').obs;
  var users = List<User>.empty().obs;

  void snackBarError(String msg) {
    Get.snackbar(
      "ERROR",
      msg,
      duration: Duration(seconds: 2),
    );
  }

  void updateState(String value, String field) {
    user.update((val) {
      if (field == 'name') user.value.name = value;
      if (field == 'email') user.value.email = value;
      if (field == 'phone') user.value.phone = value;
    });
  }

  void clearState() {
    user.update((val) {
      user.value.name = '';
      user.value.email = '';
      user.value.phone = '';
    });
  }

  User userById(String id) {
    return users.firstWhere((element) => element.id == id);
  }

  void add() {
    final dt = user.value;
    if (dt.name != '' && dt.email != '' && dt.phone != '') {
      if (dt.email.toString().contains("@")) {
        var datetime = DateTime.now().toString();

        UserApi().postUser(datetime, dt.name.toString(), dt.email.toString(), dt.phone.toString()).then((value) {
          print(value.body);
          
          users.add(
            User(
              id: value.body['name'].toString(),
              name: dt.name,
              email: dt.email,
              phone: dt.phone,
            ),
          );
        });

        Get.back();
      } else {
        snackBarError("Masukan email valid");
      }
    } else {
      snackBarError("Semua data harus diisi");
    }
  }

  void edit(String id) {
    final dt = user.value;
    print(dt.name);
    print(dt.email);
    print(dt.phone);
    if (dt.name != '' && dt.email != '' && dt.phone != '') {
      if (dt.email.toString().contains("@")) {
        var datetime = DateTime.now().toString();

        UserApi().updateUser(id, datetime.toString(), dt.name.toString(), dt.email.toString(), dt.phone.toString()).then((value) {
          print(value.body);
          final user = userById(id);

          user.name = dt.name;
          user.email = dt.email;
          user.phone = dt.phone;

          users.refresh();
        });
        
        Get.back();
      } else {
        snackBarError("Masukan email valid");
      }
    } else {
      snackBarError("Semua data harus diisi");
    }
  }

  Future<bool> delete(String id) async {
    bool _deleted = false;
    await Get.defaultDialog(
      title: "DELETE",
      middleText: "Apakah kamu yakin untuk menghapus data user ini?",
      textConfirm: "Ya",
      confirmTextColor: Colors.white,
      onConfirm: () {
        UserApi().deleteUser(id).then((_) {
          users.removeWhere((element) => element.id == id);
          _deleted = true;
        });
        
        Get.back();
      },
      textCancel: "Tidak",
    );
    return _deleted;
  }
}