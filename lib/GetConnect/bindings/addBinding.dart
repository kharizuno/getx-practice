import 'package:get/get.dart';
import 'package:getx/GetConnect/controllers/addController.dart';

import '../controllers/addController.dart';

class AddUserBinding implements Bindings {
  @override
  void dependencies() {
    Get.put(AddController());
  }
}