import 'package:get/get.dart';

import '../controllers/profileController.dart';

class ProfileBinding implements Bindings {
  @override
  void dependencies() {
    Get.put(ProfileController());
  }
}