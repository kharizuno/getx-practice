import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:getx/GetConnect/controllers/profileController.dart';
import 'package:getx/GetConnect/controllers/userController.dart';
import 'package:getx/GetConnect/models/userModel.dart';

class ProfilePage extends StatelessWidget {
  final profileC = Get.find<ProfileController>();
  final userC = Get.find<UserController>();
  final String uid = Get.arguments;

  @override
  Widget build(BuildContext context) {
    final User user = userC.userById(uid);

    profileC.nameC.text = user.name!;
    userC.updateState(profileC.nameC.text, 'name');

    profileC.emailC.text = user.email!;
    userC.updateState(profileC.emailC.text, 'email');

    profileC.phoneC.text = user.phone!;
    userC.updateState(profileC.phoneC.text, 'phone');

    return Scaffold(
      appBar: AppBar(
        title: Text("PROFILE"),
        actions: [
          IconButton(
            onPressed: () => userC.delete(uid).then(
              (deleted) {
                if (deleted) Get.back();
              },
            ),
            icon: Icon(Icons.delete_forever),
          ),
        ],
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: ListView(
            children: [
              TextField(
                controller: profileC.nameC,
                onChanged: (value) => userC.updateState(value, 'name'),
                autocorrect: false,
                textInputAction: TextInputAction.next,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                ),
              ),
              Obx(() => Text('${userC.user.value.name}')),
              SizedBox(height: 15),
              TextField(
                controller: profileC.emailC,
                onChanged: (value) => userC.updateState(value, 'email'),
                autocorrect: false,
                keyboardType: TextInputType.emailAddress,
                textInputAction: TextInputAction.next,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                ),
              ),
              Obx(() => Text('${userC.user.value.email}')),
              SizedBox(height: 15),
              TextField(
                controller: profileC.phoneC,
                onChanged: (value) => userC.updateState(value, 'phone'),
                autocorrect: false,
                textInputAction: TextInputAction.done,
                keyboardType: TextInputType.phone,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                ),
                onEditingComplete: () => userC.edit(uid),
              ),
              Obx(() => Text('${userC.user.value.phone}')),
              SizedBox(height: 40),
              GetBuilder<UserController>(
                dispose: (state) => userC.clearState(),
                builder: (_) {
                  return ElevatedButton(
                    onPressed: () => userC.edit(uid),
                    child: Text("UPDATE"),
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
