import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:getx/GetConnect/controllers/addController.dart';
import 'package:getx/GetConnect/controllers/userController.dart';

class AddPage extends StatelessWidget {
  final addC = Get.find<AddController>();
  final usersC = Get.find<UserController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("ADD USER"),
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: ListView(
            children: [
              TextField(
                controller: addC.nameC,
                onChanged: (value) => usersC.updateState(value, 'name'),
                textInputAction: TextInputAction.next,
                autocorrect: false,
                decoration: InputDecoration(
                  labelText: "Full Name",
                  border: OutlineInputBorder(),
                ),
              ),
              Obx(() => Text('${usersC.user.value.name}')),
              SizedBox(height: 15),
              TextField(
                controller: addC.emailC,
                onChanged: (value) => usersC.updateState(value, 'email'),
                keyboardType: TextInputType.emailAddress,
                textInputAction: TextInputAction.next,
                autocorrect: false,
                decoration: InputDecoration(
                  labelText: "Email Address",
                  border: OutlineInputBorder(),
                ),
              ),
              Obx(() => Text('${usersC.user.value.email}')),
              SizedBox(height: 15),
              TextField(
                controller: addC.phoneC,
                onChanged: (value) => usersC.updateState(value, 'phone'),
                keyboardType: TextInputType.phone,
                textInputAction: TextInputAction.done,
                autocorrect: false,
                decoration: InputDecoration(
                  labelText: "Phone Number",
                  border: OutlineInputBorder(),
                ),
                onEditingComplete: () => usersC.add(),
              ),
              Obx(() => Text('${usersC.user.value.phone}')),
              SizedBox(height: 40),
              GetBuilder<UserController>(
                dispose: (_) => usersC.clearState(),
                builder: (_) {
                  return ElevatedButton(
                    onPressed: () => usersC.add(),
                    child: Text("ADD USER"),
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
