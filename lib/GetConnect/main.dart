import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getx/GetConnect/controllers/userController.dart';
import 'package:getx/GetConnect/views/homePage.dart';

class GetConnectx extends StatelessWidget {
  final userC = Get.put(UserController());
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: HomePage()
    );
  }
}