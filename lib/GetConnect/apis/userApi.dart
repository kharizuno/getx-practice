import 'dart:convert';
import 'package:get/get.dart';

class UserApi extends GetConnect {
  final url = 'https://sample-1c501-default-rtdb.asia-southeast1.firebasedatabase.app/';

  // Get Requset
  Future<Response> getUser(int id) {
    return get(url + 'users/$id.json');
  }

  // Post Request
  Future<Response> postUser(String date, String name, String email, String phone) {
    final body = json.encode({
      'date': date,
      'name': name,
      'email': email,
      'phone': phone
    });

    return post(url + 'users.json', body);
  }

  // Update Request
  Future<Response> updateUser(String id, String date, String name, String email, String phone) {
    final body = json.encode({
      'date': date,
      'name': name,
      'email': email,
      'phone': phone
    });

    return patch(url + 'users/$id.json', body);
  }

  // Dekete Requset
  Future<Response> deleteUser(String id) {
    return delete(url + 'users/$id.json');
  }
}