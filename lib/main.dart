import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:getx/ArrayParse/main.dart';
import 'package:getx/routes/appPage.dart';

// import 'package:getx/GetConnect/main.dart';
// import 'package:getx/DynamicUrl/main.dart';
// import 'package:getx/Navigator/main.dart';
// import 'package:getx/Workers/main.dart';
// import 'package:getx/LifeCycle/mainGetX.dart';
// import 'package:getx/StateManager/main.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      home: ArrayParse(),
      getPages: AppPage.pages,
    );
  }
}
