import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getx/Workers/models/countModel.dart';

class CountController extends GetxController {
  var count = CountModel();

  void increment() {
    count.aksi.value++;
  }

  void reset() {
    count.aksi.value = 0;
  }

  void onInit() {
    print('onInit');
    //...workers

    // Callback
    ever(count.aksi, (_) {
      print('dijalankan');
    });

    // everAll([count.aksi], (_) {
    //   print('dijalankan');
    // });

    // once(count.aksi, (_) {
    //   print('dijalankan');
    // });

    // debounce(
    //   count.aksi,
    //   (_) => print('dijalankan'),
    //   time: Duration(seconds: 3),
    // );

    // interval(
    //   count.aksi,
    //   (_) => print('dijalankan'),
    //   time: Duration(seconds: 3),
    // );

    super.onInit();
  }
}
