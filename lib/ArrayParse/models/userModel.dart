import 'dart:convert';
import 'dart:isolate';

import 'package:get/get.dart';

class UserModel {
  String nama;
  int? umur;
  List<String>? data;
  SubObjectModel? sub_object;
  List<SubArrayModel>? sub_array;

  UserModel({required this.nama, this.umur, this.data, this.sub_object, this.sub_array});

  factory UserModel.fromJson(Map<String, dynamic> parsedJson) {
    // type 'List<dynamic>' is not a subtype of type 'List<String>?'
    // print(parsedJson['data'].runtimeType);

    // How to Convert to List String
    // # Metode 1;
    List<String> dataList1 = new List<String>.from(parsedJson['data']);

    print(dataList1);
    print(dataList1.runtimeType);
    print('-----');

    // # Metode 2;
    List<String> dataList2 = parsedJson['data'].cast<String>();

    print(dataList2);
    print(dataList2.runtimeType);
    print('-----');

    final sub_array_p = parsedJson['sub_array'] as List<dynamic>?;
    final sub_array_pp = sub_array_p != null ? sub_array_p.map((val) => SubArrayModel.fromJson(val)).toList() : <SubArrayModel>[];

    return UserModel(
      nama: parsedJson['nama'],
      umur: parsedJson['umur'],
      data: dataList1,
      sub_object: SubObjectModel.fromJson(parsedJson['sub_object']),
      sub_array: sub_array_pp
    );
  }
}

class SubObjectModel {
  String nn;
  int harga;

  SubObjectModel({required this.nn, required this.harga});

  factory SubObjectModel.fromJson(Map<String, dynamic> dt) {
    return SubObjectModel(nn: dt['nn'], harga: dt['harga']);
  }
}

class SubArrayModel {
  String nn;
  int harga;

  SubArrayModel({required this.nn, required this.harga});

  factory SubArrayModel.fromJson(Map<String, dynamic> dt) {
    return SubArrayModel(nn: dt['nn'], harga: dt['harga']);
  }
}

class UserParser {
  final String encodedJson;
  UserParser(this.encodedJson);

  Future<List<UserModel>> parseInBackground() async {
    final p = ReceivePort();
    await Isolate.spawn(_decodeAndParseJson, p.sendPort);
    return await p.first;
  }

  Future<void> _decodeAndParseJson(SendPort p) async {
    final jsonData = jsonDecode(encodedJson);
    final resultJson = jsonData['results'] as List<dynamic>;
    final results = resultJson.map((json) => UserModel.fromJson(json)).toList();

    Isolate.exit(p, results);
  }
}
