import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getx/ArrayParse/controllers/userController.dart';

class ArrayParse extends StatelessWidget {
  final userC = Get.put(UserController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            GetBuilder<UserController>(
              builder: (_) {
                return ElevatedButton(
                  onPressed: () => _.sampleJson(),
                  child: Text(
                    'Sample Array',
                    style: TextStyle(
                      color: Colors.black45,
                    ),
                  ),
                  style: ElevatedButton.styleFrom(primary: Colors.grey[300]),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
