import 'dart:convert';

import 'package:get/get.dart';
import 'package:getx/ArrayParse/models/userModel.dart';

class UserController extends GetxController {
  var userM = UserModel(nama: '').obs;
  
  // @override
  // void onInit() {
  //   super.onInit();
  //   sampleJson();
  // }

  void sampleJson() {
    List userlist = [];
    List<String> userdata = [];
    List<UserModel> usermod = [];
    
    // # Sample Data JSON using -> List = []
    print('List Dynamic');

    userlist.add({'nama': 'aa', 'umur': 26});

    print(userlist);
    print(userlist[0]['nama']);
    print(userlist.runtimeType);

    userlist[0].addAll({'nama': 'ab', 'umur': 30});

    print('# Object Assign Index 0');
    print(userlist);

    print('# Object Loop');
    userlist.forEach((item) {
      print(item['nama']);
    });
    
    print('-----------');

    // # Sample Data JSON using -> List<String> = []
    print('List String');

    userdata.add('apple');

    print(userdata);
    print(userdata[0]);
    print(userdata.runtimeType);

    print('-----------');

    // # Sample Data JSON using -> List<NamaModel> = []
    print('List Model');

    usermod.add(UserModel(nama: 'aa', umur: 26));
    usermod.add(UserModel(nama: 'ab', umur: 30));

    print(usermod);
    print(usermod[0].nama);
    print(usermod.runtimeType);

    usermod[0] = UserModel(nama: 'ab', umur: 30);

    print('# Object Assign Index 0');
    print(usermod[0].nama);

    print('# Object Loop');
    usermod.forEach((item) {
      print(item.nama);
    });

    final Map<String, UserModel> usermodMap = new Map();
    print('# Create New Object Collection');
    usermod.forEach((item) {
      usermodMap[item.nama] = item;
    });

    print(usermodMap);
    print('# Object Replace Duplicate From New Collection');
    usermod = usermodMap.values.toList();
    usermod.forEach((item) {
      print(item.nama);
    });

    print('-----------');

    // # Sample Data JSON using -> Factory NamaModel.fromJson(Map<String, dynamic> parsedJson) {}
    print('Factory Model');

    String jsonString = '{"nama": "aa", "umur": 26, "data": ["apple", "banana"], "sub_object": {"nn": "tomat", "harga": 1000}, "sub_array": [{"nn": "apple", "harga": 1000}, {"nn": "nanas", "harga": 2000}]}';

    print(jsonString);
    print(jsonString.runtimeType);
    print('-----');

    final jsonResponse = json.decode(jsonString);

    print(jsonResponse);
    print(jsonResponse.runtimeType);
    print('-----');

    var users = UserModel.fromJson(jsonResponse);

    print(users);
    print(users.sub_object);
    print(users.sub_array);
    print(users.runtimeType);

    // users = UserModel(nama: 'ab', umur: 30);

    // print(users.nama);
    // print(users.data);
    // print(users.runtimeType);

    print('-----');
  }
}