import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getx/StateManager/controllers/peopleController.dart';

class StateManager extends StatelessWidget {
  final peopleC = Get.put(PeopleController());

  // var count = 0.obs;
  // void add() {
  //   count++;
  // }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Obx(
                () => Text(
                  'Nama saya ${peopleC.xpeople.value.xname}',
                  style: const TextStyle(
                    fontSize: 35,
                  ),
                ),
              ),

              GetX<PeopleController>(
                init: PeopleController(),
                initState: (_) {},
                builder: (_) {
                  return Text(
                    'Umur saya ${_.people.age.value}',
                    style: const TextStyle(
                      fontSize: 35,
                    ),
                  );
                },
              ),
              
              GetBuilder<PeopleController>(
                init: PeopleController(),
                initState: (_) {},
                builder: (_) {
                  return Text(
                    'Umur saya ${_.people.age.value}',
                    style: const TextStyle(
                      fontSize: 35,
                    ),
                  );
                },
              ),

              // GET BUILDER WITH UNIQUE ID
              GetBuilder<PeopleController>(
                id: 'Aan',
                init: PeopleController(),
                initState: (_) {},
                builder: (_) {
                  return Text(
                    'Umur saya ${_.people.age.value}',
                    style: const TextStyle(
                      fontSize: 35,
                    ),
                  );
                },
              ),
            ],
          ),
        ),

        // State Manager Reactive with OBX
        // floatingActionButton: FloatingActionButton(
        //   onPressed: () {
        //     return peopleC.xchangeUpperCase();
        //   },
        // ),

        // State Manager Reactive with GETX
        // floatingActionButton: FloatingActionButton(
        //   onPressed: () {
        //     Get.find<PeopleController>().incrementGetX();
        //   },
        // ),

        // State Manager Simple with GET BUILDER
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            peopleC.incrementGetBuilderUniqID();
          },
        ),
      ),
    );
  }
}
