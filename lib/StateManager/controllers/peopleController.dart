import 'package:get/get.dart';
import 'package:getx/StateManager/models/peopleModel.dart';

class PeopleController extends GetxController {
  var people = PeopleModel();

  void changeUpperCase() {
    people.name.value = people.name.value.toUpperCase();
  }

  void incrementGetX() {
    people.age.value++;
  }

  void incrementGetBuilder() {
    people.age.value++;
    update();
  }

  void incrementGetBuilderUniqID() {
    people.age.value++;
    update(['Aan']);
  }

  var xpeople = PeopleModel(xname: 'Aan', xage: '25').obs;

  void xchangeUpperCase() {
    xpeople.update((val) {
      xpeople.value.xname = xpeople.value.xname.toString().toUpperCase();
    });
  }

  void xincrement() {
    xpeople.update((val) {
      xpeople.value.xage++;
    });
  }
}
